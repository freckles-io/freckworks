# -*- coding: utf-8 -*-
import os

from appdirs import AppDirs

from dictlets.defaults import DICTLETS_DEFAULT_PROTOTYPE_PATH

retailiate_app_dirs = AppDirs("freckworks", "frkl")

FRECKWORKS_WORK_DIR = retailiate_app_dirs.user_data_dir
FRECKWORKS_STATE_DIR = os.path.join(FRECKWORKS_WORK_DIR, "state")

FRECKWORKS_DEFAULT_PROTOTYPE_PATH = os.path.join(
    os.path.dirname(__file__), "resources", "prototypes"
)

DEFAULT_FRECKWORKS_TASK_MANAGER = "simple"

FRECKWORKS_PROJECT_CONFIG = {
    "project_type": "freckworks",
    "project_name": "freckworks",
    "task_manager": DEFAULT_FRECKWORKS_TASK_MANAGER,
    "prototype_repos": [
        FRECKWORKS_DEFAULT_PROTOTYPE_PATH,
        DICTLETS_DEFAULT_PROTOTYPE_PATH,
    ],
    "freckles_context_config": ["dev"],
}

FRECKWORKS_PROJECT_CONFIG_NAME = "freckworks.yml"

FRECKWORKS_FRECKLETS_PATH = os.path.join(
    os.path.dirname(__file__), "resources", "frecklets"
)
