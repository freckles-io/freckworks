# -*- coding: utf-8 -*-
import click
from ruamel.yaml import YAML

from dictlets.cli.wrapper import DictletCli
from freckworks.defaults import FRECKWORKS_PROJECT_CONFIG
from frutils.frutils_cli import logzero_option

yaml = YAML()


@click.group()
@logzero_option()
@click.pass_context
def cli(ctx):
    pass


@cli.group(name="debug")
@click.pass_context
def debug(ctx):

    pass


command_map = {"init": cli, "__default__": debug}

DictletCli.add_dictlets_commands(
    project_config=FRECKWORKS_PROJECT_CONFIG, command_map=command_map
)
