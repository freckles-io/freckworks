provider "lxd" {
}

resource "lxd_container" "machines" {
  name      = "${var.base_machine_name}-${count.index}"
  image     = "${var.image_name}"
  ephemeral = false

  count = var.number_machines

//  provisioner "local-exec" {
//    command = "frecklecute -v ${var.vars_path}/vars_master.yml -r ${var.frecklet_path} -c freckles-dev -c callback=default -t lxd::server-0 master-provisioning --consul-bootstrap-expect ${var.number_additional_servers + 1} --nomad-bootstrap-expect ${var.number_additional_servers + 1}"
//  }

  config = {
    "boot.autostart" = true
    "security.nesting" = true
  }

  limits = {
    cpu = 2
  }
}
