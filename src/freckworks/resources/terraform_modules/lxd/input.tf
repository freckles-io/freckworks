

variable "base_machine_name" {
  type = string
  default = "server"
}

variable "image_name" {
  type = string
  default = "warebase-base"
}

variable "number_machines" {
  type = number
  default = 1
}
