# -*- coding: utf-8 -*-
from dictlets.plugins.dictlet_types.callable import CallableDictlet
from freckles import Freckles
from freckles.context.run_config import FrecklesRunConfig
from freckles.exceptions import FrecklesRunException
from freckles.frecklet.vars import VarsInventory
from pyckles import Pyckles


class FrecklesDictlet(CallableDictlet):
    def __init__(
        self,
        id: str = None,
        target: str = None,
        input=None,
        multiplier=None,
        preprocess=None,
        postprocess=None,
        meta=None,
    ):

        # self._frecklet_name = frecklet_name
        # self._context_config = context_config
        # self._extra_repos = extra_repos

        super(FrecklesDictlet, self).__init__(
            callable=self.process_task,
            id=id,
            target=target,
            input=input,
            multiplier=multiplier,
            preprocess=preprocess,
            postprocess=postprocess,
            meta=meta,
        )

    # def create_pycklet(self, pyckles, _frecklet_name, **vars):
    #
    #     pycklet = pyckles.create_pycklet(_frecklet_name=_frecklet_name, **vars)
    #     return pycklet

    def create_run_config(self, target_string=None, run_alias=None):

        if run_alias is None:
            run_alias = self.alias
        metadata = {"run_alias": run_alias}
        run_config = FrecklesRunConfig(target_string=target_string, metadata=metadata)

        return run_config

    # def run_pycklets(
    #     self,
    #     pyckles,
    #     *pycklets,
    #     inventory=None,
    #     run_config=None,
    #     no_exception=False,
    #     return_run_details=False
    # ):
    #
    #     return pyckles.run_pycklets(
    #         *pycklets,
    #         inventory=inventory,
    #         run_config=run_config,
    #         no_exception=no_exception,
    #         return_run_details=return_run_details
    #     )

    def process_task(self, input):

        cc = input.get("context_config", [])
        cc.insert(0, "callback=silent")

        freckles = Freckles(
            context_config=cc, extra_repos=self.input.get("extra_repos", None)
        )

        # TODO: check if user needs to be able to set that
        elevated = None

        fx = freckles.create_frecklecutable(input["frecklet_name"])

        run_config = self.create_run_config(
            input.get("run_config", None), self._run_meta["task_name"]
        )

        inventory = VarsInventory(input.get("vars", {}))
        run_result = fx.run_frecklecutable(
            inventory=inventory, run_config=run_config, run_vars={}, elevated=elevated
        )

        if not run_result.success:
            raise FrecklesRunException(
                msg="Run for dictlet '{}' failed.".format(self.alias),
                run_info=run_result,
            )

        return run_result.result

    def _get_dictlet_type_name(self) -> str:
        return "frecklet"


class PycklesDictlet(CallableDictlet):
    def __init__(
        self,
        id: str = None,
        target: str = None,
        input=None,
        multiplier=None,
        preprocess=None,
        postprocess=None,
        meta=None,
    ):

        # self._frecklet_name = frecklet_name
        # self._context_config = context_config
        # self._extra_repos = extra_repos

        super(PycklesDictlet, self).__init__(
            callable=self.process_task,
            id=id,
            target=target,
            input=input,
            multiplier=multiplier,
            preprocess=preprocess,
            postprocess=postprocess,
            meta=meta,
        )

    def create_pycklet(self, pyckles, _frecklet_name, **vars):

        pycklet = pyckles.create_pycklet(_frecklet_name=_frecklet_name, **vars)
        return pycklet

    def create_run_config(self, target_string=None, run_alias=None):

        if run_alias is None:
            run_alias = self.alias
        metadata = {"run_alias": run_alias}
        run_config = FrecklesRunConfig(target_string=target_string, metadata=metadata)

        return run_config

    def run_pycklets(
        self,
        pyckles,
        *pycklets,
        inventory=None,
        run_config=None,
        no_exception=False,
        return_run_details=False
    ):

        return pyckles.run_pycklets(
            *pycklets,
            inventory=inventory,
            run_config=run_config,
            no_exception=no_exception,
            return_run_details=return_run_details
        )

    def process_task(self, input):

        _pyckles = Pyckles(
            "freckworks_pyckles_{}".format(self.alias),
            pyckles_context_type="python",
            context_config=input.get("context_config", []),
            extra_repos=input.get("extra_repos", None),
        )

        frecklet = self.create_pycklet(
            _pyckles, input["frecklet_name"], **input.get("vars", {})
        )
        run_config = self.create_run_config(
            input.get("target", None), self._run_meta["task_name"]
        )
        result = self.run_pycklets(_pyckles, frecklet, run_config=run_config)
        return result

    def _get_dictlet_type_name(self) -> str:
        return "pyckles"
