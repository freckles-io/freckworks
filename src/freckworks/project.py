# -*- coding: utf-8 -*-
import copy
import os

from dictlets.project import DictletsProject
from dictlets.utils import generate_random_id
from freckles import Freckles


class FreckworksProject(DictletsProject):
    def __init__(
        self,
        project_base_path=None,
        project_id=None,
        project_name=None,
        freckles_context_config=None,
        freckles_extra_repos=None,
        state_manager=None,
        task_manager=None,
        prototype_repos=None,
    ):

        if project_base_path is None:
            project_base_path = os.getcwd()
        self._project_base_path = os.path.expanduser(project_base_path)
        self._context_config = freckles_context_config
        self._extra_repos = freckles_extra_repos

        self._freckles = Freckles(
            context_config=self._context_config, extra_repos=self._extra_repos
        )

        if state_manager is None:
            state_manager = {
                "type": "folder",
                "path": os.path.join(self._project_base_path, ".freckworks"),
            }

        if project_id is None:
            # project_id = os.path.basename(self._project_base_path)
            project_id = "default"
        if project_name is None:
            # project_name = os.path.basename(self._project_base_path)
            project_name = project_id

        super(FreckworksProject, self).__init__(
            project_id=project_id,
            project_name=project_name,
            state_manager=state_manager,
            task_manager=task_manager,
            prototype_repos=prototype_repos,
            is_locked=False,
        )

    def add_dictlet(
        self,
        prototype=None,
        id=None,
        input=None,
        type=None,
        target=None,
        multiplier=None,
        meta=None,
        preprocess=None,
        postprocess=None,
    ):

        try:
            dictlet = DictletsProject.add_dictlet(
                self,
                prototype=prototype,
                input=input,
                id=id,
                type=type,
                target=target,
                multiplier=multiplier,
                meta=meta,
                preprocess=preprocess,
                postprocess=postprocess,
            )

            return dictlet
        except (ValueError):
            # fine, we'll check whether we can find a frecklet...
            pass

        frecklet = self._freckles.get_frecklet(prototype)
        if frecklet is None:
            raise ValueError(
                "No dictlet/frecklet prototype found with name: {}".format(prototype)
            )

        args = {}
        for arg_name, arg in frecklet.vars.items():
            args[arg_name] = arg.to_dict()

        input_new = {}
        input_new["frecklet_name"] = prototype
        if self._context_config:
            input_new["context_config"] = self._context_config
        if self._extra_repos:
            input_new["extra_repos"] = self._extra_repos

        input_new["run_config"] = input.get("run_config", "localhost")
        input_new["vars"] = input.get("vars", {})

        doc = frecklet.doc.exploded_dict()

        if id:
            d_id = id
        else:
            d_id = (generate_random_id(prefix=prototype),)

        if not meta:
            meta = {}
        else:
            meta = copy.copy(meta)
            meta["args"] = args
            meta["doc"] = doc

        if not meta.get("alias", None):
            if id:
                meta["alias"] = id
            else:
                meta["alias"] = prototype

        dictlet_config = {}
        dictlet_config["id"] = d_id
        dictlet_config["type"] = "frecklet"
        dictlet_config["input"] = input_new
        dictlet_config["meta"] = meta

        if multiplier:
            dictlet_config["multiplier"] = multiplier
        if preprocess:
            dictlet_config["preprocess"] = preprocess
        if postprocess:
            dictlet_config["postprocess"] = postprocess

        if target:
            dictlet_config["target"] = target

        dictlet = self._state_manager.add_dictlet(dictlet_config=dictlet_config)
        self._root_dictlet.add_dictlet(dictlet)
