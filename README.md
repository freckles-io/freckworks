[![PyPI status](https://img.shields.io/pypi/status/freckworks.svg)](https://pypi.python.org/pypi/freckworks/)
[![PyPI version](https://img.shields.io/pypi/v/freckworks.svg)](https://pypi.python.org/pypi/freckworks/)
[![PyPI pyversions](https://img.shields.io/pypi/pyversions/freckworks.svg)](https://pypi.python.org/pypi/freckworks/)
[![Pipeline status](https://gitlab.com/frkl/freckworks/badges/develop/pipeline.svg)](https://gitlab.com/frkl/freckworks/pipelines)
[![Code style](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/ambv/black)

# freckworks

*A generic, freckles-based app environment framework.*


## Description

*freckworks* is a proof-of-concept project to show how *freckles* can help reduce complexities in DevOps-type projects.

## Usage

Download the ``freckworks`` binary from: https://s3-eu-west-1.amazonaws.com/dev.dl.frkl.io/linux-gnu/freckworks, make it executable and add it to your path. For example:

```console
wget https://s3-eu-west-1.amazonaws.com/dev.dl.frkl.io/linux-gnu/freckworks
chmod +x freckworks
mkdir -p ~/.local/bin
mv freckworks ~/.local/bin

# adjust PATH (optional)
echo 'export PATH=$PATH:~/.local/bin' >> "~/.profile"
source ~/.profile
```



# Development

Assuming you use [pyenv](https://github.com/pyenv/pyenv) and [pyenv-virtualenv](https://github.com/pyenv/pyenv-virtualenv) for development, here's how to setup a 'freckworks' development environment manually:

    pyenv install 3.7.3
    pyenv virtualenv 3.7.3 freckworks
    git clone https://gitlab.com/frkl/freckworks
    cd <freckworks_dir>
    pyenv local freckworks
    pip install -e .[develop,testing,docs]
    pre-commit install


## Copyright & license

Please check the [LICENSE](/LICENSE) file in this repository (it's a short license!), also check out the [*freckles* license page](https://freckles.io/license) for more details.

[Parity Public License 6.0.0](https://licensezero.com/licenses/parity)

[Copyright (c) 2019 frkl OÜ](https://frkl.io)
