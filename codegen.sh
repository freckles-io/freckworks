#!/usr/bin/env bash

THIS_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

freckles -c freckles-dev -r ${THIS_DIR}/src/freckworks/external/frecklets codegen python-sources --package-name freckworks_frecklets -d --python-3 ${THIS_DIR}/src
